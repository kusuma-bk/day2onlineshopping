package com.org.e_commerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class ProductDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long productDetailsId;
	private long productId;
	private int quantity;
	private int productRating;

}
