package com.org.e_commerce.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class Purchase {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long purchaseId;
	private long productId;
	private long customerId;
	private int quantity;
	private int customerRating;
	LocalDateTime purchasedDate;
	

	
	

}
