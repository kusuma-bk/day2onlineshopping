package com.org.e_commerce.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorResponse {
	private String message;
	private int status;
}
