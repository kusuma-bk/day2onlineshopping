package com.org.e_commerce.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.org.e_commerce.dto.CustomerLoginResponseDto;
import com.org.e_commerce.dto.LoginRequestDto;
import com.org.e_commerce.exception.UserExistsErrorException;
import com.org.e_commerce.exception.UserPasswordErrorException;
import com.org.e_commerce.service.CustomerService;


@RestController
@CrossOrigin(origins ="*", allowedHeaders = "*")
public class LoginController {

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	CustomerService customerService;

	@PostMapping(value = "/login")
	public ResponseEntity<CustomerLoginResponseDto> checkLoginByUserId(@RequestBody LoginRequestDto loginRequestDto)
			throws UserExistsErrorException, UserPasswordErrorException {
		logger.info("Inside CustomerController of customerLogin method ");
		return new ResponseEntity<>(customerService.customerLogin(loginRequestDto), HttpStatus.ACCEPTED);
	}

}
