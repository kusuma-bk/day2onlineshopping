package com.org.e_commerce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginRequestDto {
	private String emailId;
	private String password;
}
